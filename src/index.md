---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "14 juin 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Stage Publication web avec GitLab - Support
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

14 juin 2022

- [Introduction](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/introduction.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/introduction.pdf)
- [Les générateurs de sites statiques](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/ssg.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/ssg.pdf)
- [Publier son site web avec GitLab](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/cicd.html) - [PDF](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/cicd.pdf)
