---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "14 juin 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Stage Publication web avec GitLab
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

31 mai 2022

---
## D'où je parle

* Ingénieur de recherche CNRS, expert en calcul scientifique...
* ...Au sein de l'UAR GRICAD...
* ...Responsable de l'équipe Calcul Scientifique et Données
* Formateur pour le CED UGA ; 
* **Cette formation : première pour moi !** Pas sûr du timing...
* Très faible connaissance de windows

---
## Déroulement

* Cette introduction - *1h MAX*
* **Les générateurs de sites web statiques** Cours et Mise en pratique - *2h30*
* **Publier un site web avec GitLab** Cours et Mise en pratique - *2h30* 
* Mélange cours magistral (un tout petit peu) et mise en pratique (beaucoup) : 
  * **Ne surtout pas hésitez à m'interrompre**
  * **Expérimentez !**
  * L'interactivité est la réelle plus-value du stage

---
## Mise en pratique et objectifs

* Méthodologie pour la mise en pratique
  * D'abord, vous essaierez/chercherez par vous-mêmes (je vous donnerai des indications)
  * Je montrerai une *bonne* façon de faire
  * Je vous laisserai du temps pour finir/corriger 
* **Objectif** : 
  * Créer un site web !
  * Utiliser git pour gérer son développement 
  * Le publier grâce à gitlab et le déploiement continu

--- 
## Tour de salle

- Vos noms et prénoms, bien sûr
- Votre fonction
- Votre connaissance de l'informatique (bureautique, développement, connaissance du terminal, etc.)
- Dans quel cadre pensez-vous avoir besoin de Git et GitLab ?

---
# Vérification des prérequis

---
## Les outils

- Installation de git et d'un éditeur de texte ? 

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- Utilisation basique d'un terminal ? 

### Sous windows

On utilisera VSCode.

### Sous Linux/Mac OS

On utilisera l'application/le logiciel `terminal` ou `Terminal.app`. 

---
## Utilisation basique d'un terminal - Définitions

- Quelques définitions très simples : 
  - **Terminal** : interface textuelle pour la saisie de commandes.
  - **Commandes** : Logiciels avec une interface textuelle. Requiert souvent en entrée des **arguments** (*e.g.* chemins vers des fichiers et/ou dossiers) et des options.   
```shell
$ cmd --nomoption1 -nopt2 valopt2 <argument1> <argument2> # Exemple d'appel d'un cmd avec 2 options et 2 arguments
```
*`$` représente le prompt. Ne le recopiez pas. Le texte situé après `#` est un commentaire. Ne le recopiez pas non plus.*

***Lorsque qu'un bloc de code comme ci-dessus est présent, n'hésitez pas à taper les commandes dans votre terminal. Je laisserai toujours un peu de temps pour le faire.***

---
## Utilisation basique d'un terminal - commandes utiles

```shell
$ pwd # Renvoie le chemin vers le dossier dans lequel vous êtes dans le terminal (i.e. dossier courant)
$ ls # Liste les fichiers et dossiers du dossier dans lequel vous êtes
$ ls . # Qu'est-ce que ça fait ?
$ mkdir nouveau_dossier # Créer le dossier nommé nouveau dossier
$ cd nouveau_dossier
$ touch un_fichier_vide.txt # Créé le fichier nommé unfichier_vide.txt qui est... vide
$ touch un_autre.txt
$ ls
$ rm un_fichier_vide.txt # Supprime le fichier précédemment créé
$ cd .. # change de dossier courant en remontant d'un cran dans l'arborescence (les ..)
$ pwd
$ rm -r nouveau_dossier # Supprime le dossier nommé nouveau dossier (et ce qu'il contient). 
$ cd --help # Affiche l'aide de la commande cd. Ici, --help est une option de la commande cd 
```

Les commandes peuvent avoir des options, préfixées le plus souvent par `--` pour leur nom complet ou `-`pour leur nom abrégé. 

---
## Éditer un fichier depuis le terminal

### Sous windows avec VSCode

Dans le terminal VSCode : 
```shell
$ code chemin\vers\le\fichier.txt
```

### Sous Linux/Mac OS

Dans un terminal, plusieurs solutions en fonction des éditeurs installés : 
```shell
$ code chemin\vers\le\fichier.txt
$ gedit chemin\vers\le\fichier.txt
$ nano chemin\vers\le\fichier.txt # Ou vim, emacs pour les plus téméraires :)
$...
```

---
## Afficher rapidement le contenu d'un fichier texte
 
```shell
$ cat fichier.txt
$ less fichier.txt # alternative valable sous linux et mac os
```

---
## Home sweet home

Le `/home` est le répertoire le plus haut de l'utilisateur courant. Au-dessus, ce sont des répertoires systèmes :  
 
```shell

$ cd C:\Users\pa # Sous windows
$ cd /home/pa # Sous Linux/Mac OS
$ cd # Dans les deux cas
$ cd ~ # Dans les deux cas
$ ls -a # Petit bonus pour les fichiers cachés
```

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- ~~Utilisation basique d'un terminal ?~~
- Création d'un compte gitlab ? (Gricad-GitLab en maintenance de 12h30 à 14h...)

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- ~~Utilisation basique d'un terminal ?~~
- ~~Création d'un compte gitlab ?~~ 
- [Installation d'Hugo ?](https://gohugo.io/getting-started/installing/)

---
## Les outils

- ~~Installation de git et d'un éditeur de texte ?~~
- ~~Utilisation basique d'un terminal ?~~
- ~~Création d'un compte gitlab ?~~ 
- ~~[Installation d'Hugo ?](https://gohugo.io/getting-started/installing/)~~

[**On y va ?**](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/ssg.html) 



