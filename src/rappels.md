---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "14 juin 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Rappels Git et GitLab
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

14 juin 2022

---