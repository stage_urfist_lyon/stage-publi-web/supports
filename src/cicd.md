---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "14 juin 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Publier son site web avec GitLab
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

14 juin 2022

---
# GitLab sera votre hébergeur

* GitLab est déjà un site web, il y a donc un serveur web à proximité
* GitLab sait très bien gérer les fichiers texte. Or, le HTML (entre autres), c'est du code, donc du texte. 
* GitLab a une fonctionnalité bien pratique : l'intégration et le déploiement continus
---
# Quelques rappels sur Git

Pour initialiser un dépôt à partir d'un dossier déjà existant : 
```shell
$ cd path/vers/un/dossier_rempli
$ git init .
$ git branch -m main # Si la branche principale s'appelle master
$ git add .
$ git commit -m "Ma première version"
$ ... # On modifie des fichiers existants
$ git commit -am "Ma deuxième version"
$ touch new_file.txt # On créée un fichier
$ git add new_file.txt
$ git commit -m "Ma troisième version"
```

---
# Travaux pratiques

* Créez un dépôt dans le dossier de votre site web
* Ajouter tous les fichiers dans la zone de staging (index)
* Faites votre premier commit

--- 
# Quelques rappels sur GitLab

[Démo en ligne](https://gricad-gitlab.univ-grenoble-alpes.fr/)

---
# Travaux pratiques 

* Créez un projet nommé `votrelogin-site` (**vide**, sans README) dans le groupe Stages URFIST Lyon/Stage Publi Web
* Suivez les instructions qui apparaissent sur la page du projet pour "pousser" le code de votre site sur ce projet gitlab

--- 
# L'intégration continue et le déploiement continu CI/CD

La CI/CD consiste déclencher un ensemble de processus à chaque modification du code source, par la mise en place d'une chaîne d'exécution automatique : 
- Des tests (unitaires, fonctionnels, performance) : **intégration continue**
- De la génération de données : documentation, html, etc : **déploiement continu**

Des outils comme github ou gitlab propose un outil CI/CD relativement simple à utiliser et très fonctionnel.

---
# CI/CD

Concrètement, à chaque `git push` vers GitLab, des tâches prédéfinies par les développeurs sont exécutées

![h:400 center](fig/cicd.png)

---
# CI/CD en pratique

* Sur GitLab, il faut définir des **runners/exécuteurs** (voir démo) pour le projet
* Ensuite, à la racine de notre dépôt, il faut créer un fichier nommé `.gitlab-ci.yml` à la racine du dépôt. Il contient la liste des tâches à effectuer : quelles actions, sur quelles machines, dans quel ordre 
* Dès que ce fichier est présent et les runners configurés, la CI/CD se déclenche automatiquement

--- 
# Le .gitlab-ci.yml 

[Voir exemple](https://gricad-gitlab.univ-grenoble-alpes.fr/stage_urfist_lyon/git/supports/-/blob/main/.gitlab-ci.yml)

---
# Passons à la pratique

* Tout d'abord, on active le **runner** → Suivez le guide
* Ensuite, on créée le `.gitlab-ci.yml` → Suivez le guide
* Allons observer le pipeline de tâche en cours d'exécution

---
# Et c'est tout ? 

* Normalement, votre site web est créée et publié
* Vous pouvez avoir son URL en allant dans les paramètres de votre projet GitLab et dans la rubrique **Pages** 
* Tout va bien ? 

---
# Retour à la config

* `git pull` !
* Bien mettre la bonne URL dans `config.toml`
* `git add, git commit, git push`
* On vérifie le pipeline (dernière fois, promis)
* Alors, ce site ? 

---
# Encore un oubli ;)

Ne pas oublier de passer la métadonnée de vos posts/pages à `false` !

---
# Quelques remarques

* Vous ne maitrîsez pas la CICD, vous savez juste ce que c'est, pour le moment :)
* L'URL de votre site vous paraît un peu longue/absconse. Sur le GitLab de GRICAD, on ne peut pas spécifier son propre nom de domaine. Sur GitLab.com ou Github, oui (et c'est assez simple si on possède déjà un nom de domaine). Quid du Gitlab huma-num ? 
* Maintenant, pour poster un nouvel article, une nouvelle page, vous créer un fichier au bon endroit, vous écrivez, vous faites `git add, git commit, git push` et voilà, le site est mis à jour.  

---
# The end.

Vous savez créer un site web avec Hugo et le publier sur le web. Bravo !
