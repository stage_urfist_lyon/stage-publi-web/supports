---
marp: true
theme: gricad
author: Pierre-Antoine Bouttier
paginate: true
footer: "14 juin 2022 - *pierre-antoine.bouttier@univ-grenoble-alpes.fr*"
---

# Générateurs de sites web statiques
## URFIST de Lyon 

*pierre-antoine.bouttier@univ-grenoble-alpes.fr*

14 juin 2022

---
# Un site web, pour quoi faire ? 

* Un site web permet de publier de l'information (comme un mail, un magazine, une vidéo, etc)
* Son avantage : l'accessibilité
* Quelques usages intéressants (que vous avez déjà rencontrés): 
  * Pages personnelles, d'évènement
  * Communication
  * Documentation

---
# Comment fait-on un site web ? 

* Il faut d'une part le construire... :
  * Avoir du contenu (texte, audio, vidéo, images, etc.)...
  * ...agencé suivant des standards qui permettent à tout le monde d'y accéder (via leurs navigateurs) : CSS (pour la forme), HTML (pour la structure)...
  * ...le tout agrémenté de fonctions supplémentaires, implémentées dans des langages de programmation : Javascript, PHP, Python, SQL, etc.  

* ...puis le mettre à disposition : 
  * À l'aide d'un serveur web : Apache, Nginx, etc
  * Qui tourne sur un ordinateur qu'il faut bien configurer...
  * ...y compris en terme de réseau

---
# Va-t-on apprendre tout ça en 6 heures ?

Non. 

Construire et publier un site web sur mesure est un métier (parfois, plusieurs).  

---
# SGC, SSG, kezako ?

Il existe des systèmes (logiciels) qui permettent de **construire** des sites webs (=pages HTML) *simplement*

* Les Systèmes de Gestion de Contenu (SGC ou CMS en anglais) : 
  * Ensemble d'outils pour créer et gérer des sites web complets et complexes
  * Souvent **dynamiques**, *i.e.* le rendu (=pages HTML) est généré lorsque le visiteur le consulte
  * Nécessitent, quasiment tout le temps, des bases de données
  * Exemples : wordpress, drupal

---
# SGC, SSG, kezako ?

* Les Générateurs de sites statiques (SSG en anglais) : 
  * Construit, en quelques commandes/scripts, un site web à partir de fichiers textes (contenu et templates/css) et medias
  * Le site construit est **statique**
  * Les données d'entrée sont stockées dans un système de fichiers
  * Exemple : **hugo**, jekyll, pelican, gatsby, eleventy

---
# Pourquoi nous intéressons-nous aux SSGs ? Pros

* Les avantages 
  * Légéreté et rapidité : Les pages HTML sont déjà écrites et stockées, il n'y a pas besoin de les construire à la volée
  * Sécurité : faible surface d'attaque
  * Contrôle de version du contenu : seuls des fichiers textes (et médias) sont nécessaires, donc on peut utiliser Git (et GitLab) pour gérer l'évolution du site. 
  * Maintenance légère : comparé à un CMS, la maintenance du SSG en lui-même est nulle. 

---
# Pourquoi nous intéressons-nous aux SSGs ? Cons

* Quand ne pas les utiliser : 
  * Contenu fortement dynamique
  * Traitement d'entrées utilisateurs (**e.g.** formulaires)
  * Nécessitent quelques compétences (mais les CMS aussi)
  * **Nota Bene** : Site statique ≠ [petit site](https://gricad-doc.univ-grenoble-alpes.fr/hpc/)

---
# Les différents SSGs

* Comme déjà mentionné, il existe pléthore de SSGs
* Ils peuvent être écrits dans différents langages : python, php, javascript, etc.
* Fonctionnement souvent semblable
* Nous utiliserons dorénavant [le SSG Hugo](https://gohugo.io/) : communauté active (plein d'avantages à ça), s'installe assez facilement sur toutes les plateformes, rapide, a pas mal de fonctionnalités, open source. 

---

# Les premiers pas

---
# Création des données d'entrée

On crée la structure de notre premier site : 

```bash
# Placez vous dans le dossier de votre choix
hugo new site mon_premier_site
```
Maintenant allez dans le dossier créé par hugo appelé `mon_premier_site`. 

---
# La structure d'un site Hugo (1/2)

* Fichier `config.toml` : principal fichier de configuration 
* Répertoire `archetypes` : modèles de contenu.
* Répertoire `content` : l'endroit où se trouve le contenu réel de votre site, stocké dans des fichiers **markdown** (ou html). Sa hiérarchie permet d'organiser également l'architecture de votre site. 
* Répertoire `layout` : fichiers HTML qui définissent la structure du site. On ne va pas s'y intéresser aujourd'hui.

---
# La structure d'un site Hugo (1/2)

* Répertoire `themes` : contiennent tous les fichiers nécessaires à l'application de thèmes à votre site (CSS, HTML, JS, etc.). Nous y reviendrons. 
* Répertoire `data` : pour stocker des donénes supplémentaires. On ne va pas s'y intéresser aujourd'hui. 
* Répertoire `static` : stockage des ressources stratiques qui ne nécessitent pas de traitement supplménetaire : (e.g. images, vidéo, son)

---
# Choisir un thème 

* La forme doit s'accorder avec le fond ! 
* En fonction de la ligne éditoriale de votre site, il faut choisir un thème adéquat : site différent pour un blog, une documentation une "landing page", etc. 
* Une galerie de thèmes : https://themes.gohugo.io/ 
* Avant d'installer le thème, lisez ses caractéristiques/documentation !

--- 
# Choisir un thème

Nous installerons le [thème populaire **Hyde**](https://themes.gohugo.io/themes/hyde/) : 
```bash
cd themes
git submodule add https://github.com/spf13/hyde.git hyde
cd ..
```

Cette commande git permet d'ajouter proprement un dépôt git à l'intérieur d'un autre dépôt git. 

---
# Premier tour dans la configuration du site web

Nous voulons maintenant dire à Hugo que nous voulons utiliser ce thème. Pour cela, il faut éditer le fichier `config.toml`, et ajouter la ligne : 

```
theme = 'hyde'
```

Nous verrons les autres paramètres plus tard. 

---
# Génération et visualisation du site web

Pour générer et visualiser, en temps réel, votre site web, il faut utiliser la commande suivante : 

```bash
hugo server -D
```
Maintenant, rendez-vous à l'adresse indiquée en retour par la commande.

À partir de maintenant, si vous ne fermez pas le terminal dans laquelle cette commande s'est exécutée, chacune de vos modifications entraînera une régénération automatique de votre site. 

---
# Création de contenu

* Comme vu précédemment, le contenu va dans le dossier `content` et sa structuration va conditionner l'architecture de vos sites.
* Dans notre, cas, on va construire un site avec des articles de blog d'une part (suite chronologique de textes) et des pages figées (À propos, CV, etc). 
* Pour ce faire, on créée un dossier `content/posts/2022`, puis un dossier `content/pages`
* Cette distinction permet d'appliquer des *layouts* (=modèles) différents au type de contenu. 
* Souvent les thèmes vous proposent différents *layouts*, en fonction de la visée du thème. Ce thème-ci ne fait pas vraiment la différence.

---
# Création d'un premier article

Pour créer son premier article, soit on créée manuellement un fichier `.md` dans `contents/posts/2022/` soit on utilise la commande : 

```bash
hugo new posts/2022/2022-06-15-premier-article.md 
```
Vous pouvez nommer le fichier comme vous le souhaitez, le format `YYYY-MM-DD-titre` est une indication. 

*Jetez un coup d'oeil sur votre site dans le navigateur*

---
# Création d'un premier article

* En ouvrant le fichier, on remarque un en-tête contenant des métadonnées : titre, date, draft
* [Il y a beaucoup de mots-clés disponibles](https://gohugo.io/content-management/front-matter/)
* Les thèmes peuvent en définir également (lisez la documentation des thèmes)

* Maintenant, vous pouvez écrire le contenu de l'article. Hugo gère le langage **Markdown** et le HTML.

--- 
# Le langage Markdown

* Au cours du stage Git/GitLab, ou ailleurs, vous avez pu voir des fichiers dont le nom se finissait par `.md` : c'est l'extension par défaut pour le markdown (parfois `.markdown` ou même `.txt`). 
* Un fichier markdown est un fichier texte brut avec quelques règles syntaxiques (langage balisé).
* Si vous écrivez un document markdown dans votre dépôt gitlab, Gitlab l'affichera formatté. 
* Il est très conseillé, pour chaque projet git/gitlab, d'écrire un fichier README.md qui décrira le projet et ce qu'il y a savoir à son propos.

---
## Le langage markdown

```markdown
# Titre de niveau 1

*Pour écrire en italique*, **pour écrire en gras**, ~~du texte barré~~.

## titre de niveau 2

voici une liste : 
- Élément 2
- Élément 1

### Titre de niveau 3

Une liste ordonnée : 
1. Élément 1
2. Élément 2
```

---
# Le langage markdown

```markdown

[En cliquant sur ce texte vous accéderez à l'URL entre parenthèse](https://...)

![Texte alternatif](/chemin/ou/url/vers/une/image.png)
```

---
# Création d'un premier article

* Remplissons l'article. 
* Mettez une image dans le dossier `static`. Insérez-la dans votre article. 

* *Jetez un coup d'oeil au site de temps en temps*

---
# Création d'une première page 

De la même manière que pour un article, vous pouvez créer manuellement un fichier `titre.md` dans le dossier `content/pages/` ou utiliser la commande :

```bash
hugo new pages/a-propos.md
```

Les pages ne sont pas liées à une date, il n'y a donc pas de convention de nommage. 

---
# Retour à la configuration

On voudrait changer le titre, la description du site, la couleur et ajouter un lien vers la page *À propos* dans la barre latérale. Comme décrit dans la documentation du thème, il faut alors éditer le fichier `config.toml` : 

```
baseURL = 'http://example.org/'
languageCode = 'en-us'
title = 'Mon nouveau site'

theme= 'hyde'

[Menus]
  main = [
      {Name = "A propos", URL = "/pages/a-propos"},
  ]

[params]
  themeColor = "theme-base-09"
  description = "Ce site est une success story"
```

---
# A vous de jouer

- Compléter votre site : créer plusieurs pages, plusieurs posts. 
- Posez vos questions sur ce que vous voudriez faire, ajouter, enlever. 

---
# Mais où sont nos pages web ? 

* Jusqu'à maintenant, nous avons visualisé le site web généré par Hugo ? Mais où est-il ?
* Nulle part :) Pour générer les fichiers qui seront publiés par un serveur web, il faut lancer la commande `hugo -D`. 
* Un nouveau dossier, dans votre projet, a été créé : `public/`. Allons y faire un tour. 

---
# Et maintenant ? Comment je le publie ? 

[Nous allons voir comment installer et configurer un serveur web apache sur un serveur dédié.](https://stage_urfist_lyon.gricad-pages.univ-grenoble-alpes.fr/stage-publi-web/supports/cicd.html)